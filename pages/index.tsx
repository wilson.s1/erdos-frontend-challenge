import { useAppDispatch, useAppSelector } from "../hooks";
import styles from "../styles/Home.module.css";
import { useEffect } from "react";
import { fetchPosts } from "../slices/postSlice";
import { selectOldPosts, selectRecentPosts } from "../slices/postSelector";

export default function Home() {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);
  const recentPosts = useAppSelector(selectRecentPosts);
  const oldPosts = useAppSelector(selectOldPosts);
  return (
    <div className={styles.container}>
      <h1>Recent Posts</h1>
      {recentPosts.map((post, index) => (
        <div key={index}>{JSON.stringify(post)}</div>
      ))}

      <h1>Old Posts</h1>
      {oldPosts.map((post, index) => (
        <div key={index}>{JSON.stringify(post)}</div>
      ))}
    </div>
  );
}
