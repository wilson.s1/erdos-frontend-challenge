import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../store";


export const selectPosts = createSelector(
  (state: RootState) => state.post.posts,
  (posts) => posts
);

export const selectRecentPosts = createSelector(
  (state: RootState) => state.post.posts,
  (posts) => posts.filter(post => post)
);

export const selectOldPosts = createSelector(
  (state: RootState) => state.post.posts,
  (posts) => posts.filter(post => post)
);
