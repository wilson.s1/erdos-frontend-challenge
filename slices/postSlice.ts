import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { client } from "../axios";
import { Post } from "./post";

export const fetchPosts = createAsyncThunk(
  "posts/fetchPosts",
  async (): Promise<Post[]> => {
    const response = await client.get("");
    return response.data.map(
      (post: { title: string; date: string; content: string }) => ({
        ...post,
        date: new Date(post.date)
      })
    );
  }
);


interface PostSlice {
  posts: Post[];
}

const initialState = { posts: [] } as PostSlice;

export const postSlice = createSlice({
  name: "post",
  initialState,
  reducers: {
    setPosts: (state, action: PayloadAction<Post[]>) => {
      state.posts = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchPosts.fulfilled, (state, action) => {
      state.posts = action.payload;
    });
  },
});